class Person {
    constructor(name, weightInKg) {
        this.name = name;
        this.weightInKg = weightInKg;
    }

    convertKiloToPounds() {
        return this.weightInKg * 2.205;
    }

    displayWeightInPounds() {
        console.log(
            `${
                this.name
            }'s weight in lbs is ${this.convertKiloToPounds().toFixed(0)}`
        );
    }
}

const jelly = new Person("Jelly", 59.8742);
jelly.displayWeightInPounds();
